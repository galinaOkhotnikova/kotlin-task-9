package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    private lateinit var image: ImageView
    private lateinit var button: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button = findViewById(R.id.button)
        image = findViewById(R.id.image)
       /* when(option) {
            1 -> button.setOnClickListener(View.OnClickListener
            {image.setImageResource(R.drawable.forapp) })
            2 -> button.setOnClickListener(View.OnClickListener
            {image.setImageResource(R.drawable.forapp1) })
            3 -> button.setOnClickListener(View.OnClickListener
            {image.setImageResource(R.drawable.forapp2) })
            4 -> button.setOnClickListener(View.OnClickListener
            {image.setImageResource(R.drawable.forapp3) })
            5 -> button.setOnClickListener(View.OnClickListener
            {image.setImageResource(R.drawable.forapp4) })
            6 -> button.setOnClickListener(View.OnClickListener
            {image.setImageResource(R.drawable.forapp5) })
        }*/
        var opt = 0
        button.setOnClickListener {
            var option = (1..6).random()
            while (option == opt) {
                option = (1..6).random()
            }
            when (option) {
                1 -> image.setImageResource(R.drawable.forapp)
                2 -> image.setImageResource(R.drawable.forapp1)
                3 -> image.setImageResource(R.drawable.forapp2)
                4 -> image.setImageResource(R.drawable.forapp3)
                5 -> image.setImageResource(R.drawable.forapp4)
                6 -> image.setImageResource(R.drawable.forapp5)
            }
            opt = option
            }
        }
    }



